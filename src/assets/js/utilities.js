function isItAnEmail( str ) {
    return str.indexOf( '@' ) > -1;
}

function isItAUserName( str ) {
    let regExp = new RegExp( /^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/ );
    return regExp.test( str );
}

function isItAnAge( int ) {
    return Number.isInteger( int ) && int > -1;
}

function isItAUser( user ) {
    return user instanceof User;
}

function isItAString(string) {
	if (typeof string === 'string') {
		return true;
	} else {
		return false;
	}
}

function isItAPost (post) {
	return post instanceof Post;
}

function animateCss(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}


