let list = {
	addUserToList(email, username, age) {
		userEmail.val('');
		userName.val('');
		userAge.val('');
		animateCss('#add-user-btn', 'pulse');
		animateCss('#jumb1', 'rubberBand');
		let tempUser = new User(email);
		let newAge = parseInt(age);
		tempUser.setAge(newAge);
		tempUser.setUserName(username);
		users.push(tempUser);
		dropDownList.append(`<option value="${tempUser.userName}">${tempUser.userName}</option>`);
		friendUser.append(`<option value="${tempUser.userName}">${tempUser.userName}</option>`);
		friendFriend.append(`<option value="${tempUser.userName}">${tempUser.userName}</option>`);
		userDisplaySelect.append(`<option value="${tempUser.userName}">${tempUser.userName}</option>`);
	}, 

	addPostwUser(title, text) {
		textPost.val('');
		titlePost.val('');
		animateCss('#post-submit-btn', 'pulse');
		animateCss('#jumb2', 'rubberBand');
		let tempPost = new Post(title, text);
		let tempUser = dropDownList.val();
		for (i = 0; i < users.length; i++) {
			if (users[i].userName === tempUser) {
				tempUser = users[i];
			}
		}
		tempUser.addPost(tempPost);
	},

	addFriends() {
		animateCss('#friends-submit', 'pulse');
		animateCss('#jumb3', 'rubberBand');
		let userFriend = $('#friend-1').find(":selected").val();
		let friend = $('#friend-2').find(":selected").val();
		let userFriendO, friendO;
		if (userFriend !== friend) {
			for (i = 0; i < users.length; i++) {
				if (users[i].userName == userFriend) {
					userFriendO = users[i];
				} else if (users[i].userName == friend) {
					friendO = users[i];
				}
			}
			userFriendO.addFriend(friendO);
		}
	},

	postWDisplayer(user) {
		postDisplaySelect.html('');
		let tempUser;
		for (let i = 0; i < users.length; i++) {
			if (users[i].userName == user) {
				tempUser = users[i];
			}
		}
		for (let i = 0; i < tempUser.posts.length; i++) {
			postDisplaySelect.append(`<option value="${tempUser.posts[i].title}">${tempUser.posts[i].title}</option>`);
		}
	}
}

