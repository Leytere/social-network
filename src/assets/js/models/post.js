
/**
 * ICI est codé le modèle de Post
 * Cette classe sert à manipuler des données, PAS à les afficher !!!
 */

/**
 * La classe Post
 */
class Post {

    /**
     * Constructeur de Post
     * 
     * @param  string title
     * @param  string text
     */ 
    constructor( title, text ) {
        this.setTitle( title ); 
        this.setText( text ); 
    }

    /**
     * Récupère le title
     */
    getTitle() {
        return this.title;
    }

    /**
     * Modifie le title
     * 
     * @param string title 
     */
    setTitle( title ) {
        // On teste si title n'est pas une String
        if( ! isItAString( title ) ) {
            throw new Error(`Oh!! "${title}" is not a valid title!!!`);
        }
        // Si c'est le cas
        else {
            for (let i = 0; i < users.length; i++) {
                for (let j = 0; j < users[i].posts.length; j++) {
                    if (users[i].posts[j].title == title) {
                        throw new Error(`Oh!! This title is already taken!!!`);
                    }
                }
            }
            this.title = title;
        }
    }

    /**
     * Récupère le text
     */
    getText() {
        return this.text;
    }

    /**
     * Modifie le text
     * 
     * @param string text 
     */
    setText( text ) {
        // On teste si text n'est pas une String
        if( ! isItAString( text ) ) {
            throw new Error(`Oh!! "${text}" is not a valid text!!!`);
        }
        // Si c'est le cas
        else {
            this.text = text;
        }
    }
}